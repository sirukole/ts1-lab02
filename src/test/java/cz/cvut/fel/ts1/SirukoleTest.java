package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SirukoleTest {
    @Test
    public void factorial_nIs5_returns120(){
//        arrange
        Sirukole sir = new Sirukole();
//        act
        int actual = sir.factorial(5);
//        assert
        Assertions.assertEquals(120, actual);
    }
//    @Test
//    public void factorialTest(){
//        Sirukole sir = new Sirukole();
//        int actual = sir.factorial(5);
//        Assertions.assertEquals(120, actual);
//    }
//    @Test
//    public void factorialTest1(){
//        Sirukole sir = new Sirukole();
//        int actual = sir.factorial(3);
//        Assertions.assertEquals(6, actual);
//    }
//    @Test
//    public void factorialTest2(){
//        Sirukole sir = new Sirukole();
//        int actual = sir.factorial(0);
//        Assertions.assertEquals(1, actual);
//    }
}
